<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'quaesta_wp');

/** MySQL database username */
define('DB_USER', 'quaesta');

/** MySQL database password */
define('DB_PASSWORD', 'Quaesta%2015');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'k=mV9LFQeUGNuUf3GLWvyxpjjO_WDreopc+RZe_UMQfa+5n/zVR4Sw(LMn1GFKh+');
define('SECURE_AUTH_KEY',  'tGSPFBRz^NBr50PzfZ2hKQ/rn#0)TW=Y4nQQ^T=L^#vXmmFzLD)X6#-Yoj2z=s4D');
define('LOGGED_IN_KEY',    'FBzZ+Hd8O1v63T9L2J28jTFOu#6XEKi+bDac6z9VWJSYksl5ToFAkH+3UJY464Ma');
define('NONCE_KEY',        'l+Q=!!GRVWddv7Z/m-CVAGq#MADpLs+)sH7v9ItOC^K_wuwR0PR=Nr72(#Ww0v=7');
define('AUTH_SALT',        'rqOXw)aIGynr5u82/96(QF/Y89n4SFuFPxOZBUOLhpKQDzYJNCLHnMH5!1bQ=8#!');
define('SECURE_AUTH_SALT', 'g=gwiP2i!3JntkJdP(6d5jHUwbVA5y41)G5eT2-wbjspzlAX7+(^L64JuwujEoTT');
define('LOGGED_IN_SALT',   'EeBXuUz-a=TX5XWC(aR8pp+V65XOGhfoqjRvIcUqKZ-Dwlwk8ZCyL40B6GzGqjS4');
define('NONCE_SALT',       'gkgeH89mt^jftTBe4!RZn2n84Bt8t-YpboxK4xRumX5Kh^FhrY/Zq4S4Ye8vo1!G');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/**
 *  Change this to true to run multiple blogs on this installation.
 *  Then login as admin and go to Tools -> Network
 */
define('WP_ALLOW_MULTISITE', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/* Destination directory for file streaming */
define('WP_TEMP_DIR', ABSPATH . 'wp-content/');
