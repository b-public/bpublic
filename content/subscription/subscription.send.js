// CONTACT FORM FUNCTION
	var contact_send = function(){

	'use strict';

	var surname = $("#surname").val();
	var name  = $("#name").val();
  var email = $("#email").val();
  //var checkbox1 = $("#checkbox1").val();

	if ( surname=="" ){ alert("Bitte geben Sie einen Vornamen an."); $("#surname").focus(); }
  else if ( name=="" ){ alert("Bitte geben Sie einen Namen an."); $("#name").focus(); }
	else if ( email=="" ){ alert("Bitte geben Sie eine Emailadresse an."); $("#email").focus(); }
  //else if (document.getElementById('checkbox1').checked != 1) { alert("Bitte bestätigen Sie die Übermittlung Ihrer Kontaktdaten."); }
	else {
		$.post("content/subscription/subscription.send.php", { surname:surname, name:name, email:email}, function( result ){
			if ( result=="SUCCESS" ){
        //alert(venue);
				alert("Ihre Anmeldung wurde erfolgreich übermittelt.");
				setTimeout(function(){
					$("#name").val("");
					$("#surname").val("");
					$("#email").val("");
				}, 3000);
			} else {
				alert("Ihre Anmeldung wurde nicht übermittelt. Bitte überprüfe Sie Ihre Eingabe.");
			}
		});
	}

};
