Title: Code éthique

----

Headline: Assumer ses responsabilités

----

Text: Nous avons des opinions ou des principes qui peuvent différer. Cependant, dès lors qu’il existe un code éthique, il est des règles claires auxquelles il est facile de se rapporter. L’agence b-public est très attachée au respect des normes ainsi établies, ainsi que peut le refléter la qualité de nos travaux. Nos prestations sont soumis à différentes chartes déontologiques telles que le Code de Lisbonne, le Code d’Athènes ou encore la Charte de Stockholm de l’ICCO. Sur le plan local, nous nous référons enfin au code de l’Association Suisse pour la Communication de Crise.

----

Img: slider13.jpg

----

Subtitle: 