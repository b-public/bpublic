Title: Ethics

----

Text: 

Practising communications is directly linked with ethical norms. Our work at b-public is characterised by trust, open-mindedness and honesty. This means the transparent handling of performance as well as fees meaning we also give our clients a full and comprehensive report on all their communicational activities.
 
Ethics don't exist to limit the market and economics or to prevent the efficient functioning of the market. Ethics serve to allow a long-term partnership to flourish.

----

Headline: Appropriate action

----

Img: 

----

Subtitle: 