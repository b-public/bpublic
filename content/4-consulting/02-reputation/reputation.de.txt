Title: Reputation

----

Img: slider26.jpg

----

Headline: Wehe dem, der die Erwartungen enttäuscht

----

Text: 

Erfolgreich ist, wer die gemachten Versprechungen einhält und sich einen Reputationsvorsprung erarbeitet. Das Verhalten eines Unternehmens unterstreicht seine Identität und seinen Ruf. Eine gute Reputation trägt daher zur Maximierung eines Firmenwertes bei.

Im Zuge der Finanz- und Wirtschaftskrise ist vielerorts ein hoher Vertrauensverlust entstanden. Unternehmen versuchen deshalb neu, ihre Stärken auf verschiedenen Kanälen und in verschiedenen Medien zu vermitteln. Sie wollen, dass Kunden, Mitarbeitende und Kapitalgeber sie eindeutig identifizieren und von den Konkurrenzfirmen unterscheiden können. Wir begleiten Sie auf dem langfristigen Prozess im Aufbau der Reputation und ermöglichen Ihnen eine regelmässige Aussenansicht.

----

Subtitle: 