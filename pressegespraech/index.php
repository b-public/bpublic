<!DOCTYPE html>
<html>
	<head>
		<title>Jahres-Pressegespräch 2017</title>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

		<link rel="stylesheet" href="css/demo.css">
		<link rel="stylesheet" href="css/sky-forms.css">

		<!--[if lt IE 9]>
			<link rel="stylesheet" href="css/sky-forms-ie8.css">
		<![endif]-->

		<!--[if lt IE 10]>
			<script src="js/jquery.placeholder.min.js"></script>
		<![endif]-->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script src="js/sky-forms-ie8.js"></script>
		<![endif]-->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="js/subscription.send.js" type="text/javascript"></script>
	</head>
	<body class="bg">
		<div class="body body-s">

		<header style="display: flex; justify-content: space-between;">
			<div class="img">
				<img src="img/egonz_logo.png" width="" height="">
			</div>

		</header>

		<div style="text-aling: center; vertical-align: middle; line-height: 20px;">
			<h1>Jahres-Pressegespräch 2017</h1>
			<h2>der Egon Zehnder</h2>
		</div>

			<form class="sky-form">
				<header>Antwortkarte</header>

				<fieldset>
					<section>
						<label class="checkbox"><input type="checkbox" name="checkboxYes" id="checkboxYes"><i></i>Ich nehme am Jahres-Pressegespräch am 9. März 2017 in Zürich teil.</label>
						<label class="checkbox"><input type="checkbox" name="checkboxNo" id="checkboxNo"><i></i>Ich kann am Jahres-Pressegespräch 2017 nicht teilnehmen.</label>
					</section>
				</fieldset>
				<fieldset>
					<section>
						<label class="checkbox"><input type="checkbox" name="checkboxDocsYes" id="checkboxDocsYes"><i></i>Bitte senden Sie mir die Unterlagen.</label>
						<label class="checkbox"><input type="checkbox" name="checkboxDocsNo" id="checkboxDocsNo"><i></i>Ich wünsche keine Unterlagen.</label>
					</section>

				</fieldset>

				<fieldset>
					<section>
						<label class="input">
							<i class="icon-append icon-user"></i>
							<input type="text" placeholder="Redaktion" name="redaktion" id="redaktion" >
							<b class="tooltip tooltip-bottom-right"></b>
						</label>
					</section>

					<div class="row">
						<section class="col col-6">
							<label class="input">
								<input type="text" placeholder="Vorname" name="vorname" id="vorname" >
							</label>
						</section>
						<section class="col col-6">
							<label class="input">
								<input type="text" placeholder="Nachname" name="name" id="name" >
							</label>
						</section>
					</div>

					<div class="row">
						<section class="col col-6">
							<label class="input">
								<input type="text" placeholder="PLZ" name="plz" id="plz">
							</label>
						</section>
						<section class="col col-6">
							<label class="input">
								<input type="text" placeholder="Ort" name="ort" id="ort" >
							</label>
						</section>
					</div>

					<section>
						<label class="input">
							<i class="icon-append icon-envelope"></i>
							<input type="text" placeholder="Email" name="email" id="email" >
							<b class="tooltip tooltip-bottom-right"></b>
						</label>
					</section>

				</fieldset>

				<footer>
					<div class="button" onclick="smgsub_send();">Anmelden</div>
				</footer>
			</form>

		</div>
	</body>

</html>
