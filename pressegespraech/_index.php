<!DOCTYPE html>
<html>
	<head>
		<title>SMG Forum</title>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

		<link rel="stylesheet" href="css/demo.css">
		<link rel="stylesheet" href="css/sky-forms.css">
		<!--[if lt IE 9]>
			<link rel="stylesheet" href="css/sky-forms-ie8.css">
		<![endif]-->

		<!--[if lt IE 10]>
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
			<script src="js/jquery.placeholder.min.js"></script>
		<![endif]-->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script src="js/sky-forms-ie8.js"></script>
		<![endif]-->
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
		<script src="js/subscription.send.js" type="text/javascript"></script>
	</head>
	<body class="bg">
		<div class="body body-s">

		<!--<div>
			<div class="img">
				<img src="img/SMG_Forum_Logo.png" width="300" height="200">
			</div>

			<div class="img">
				<img src="img/SMG_Forum.jpg" width="300" height="200">
			</div>
		</div>-->

		<div style="text-aling: center; vertical-align: middle; line-height: 20px;">
			<h1>53. SMG Forum</h1>
			<h2>Forum «Upcycling - fresh perspectives on strategy»</h2>
		</div>

			<form action="" class="sky-form">
				<header>Antwortkarte</header>

				<fieldset>
					<section>
						<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Ich nehme am 53. SMG Forum am 1. September 2016 in Zürich teil.</label>
						<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Ich kann am 53. SMG Forum am 1. September 2016 nicht teilnehmen.</label>
					</section>
				</fieldset>
				<fieldset>
					<section>
						<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Bitte senden Sie mir die Unterlagen.</label>
						<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Ich wünsche keine Unterlagen.</label>
					</section>

				</fieldset>

				<fieldset>
					<section>
						<label class="input">
							<i class="icon-append icon-user"></i>
							<input type="text" placeholder="Redaktion">
							<b class="tooltip tooltip-bottom-right"></b>
						</label>
					</section>

					<div class="row">
						<section class="col col-6">
							<label class="input">
								<input type="text" placeholder="Vorame">
							</label>
						</section>
						<section class="col col-6">
							<label class="input">
								<input type="text" placeholder="Nachname">
							</label>
						</section>
					</div>

					<div class="row">
						<section class="col col-6">
							<label class="input">
								<input type="text" placeholder="PLZ">
							</label>
						</section>
						<section class="col col-6">
							<label class="input">
								<input type="text" placeholder="Ort">
							</label>
						</section>
					</div>
				</fieldset>

				<footer>
					<button type="submit" class="button" onclick="smgsub_send();">Submit</button>
				</footer>
			</form>

		</div>
	</body>

</html>
