<?php

	/* Subscription Form Setup Begin */

	$send_name      = "Jahres-Pressegespräch 2017 | Egon Zehnder";		// Replace your name
	$send_title     = "Anmeldebestätigung";		// Replace email sent title
	$send_address   = "subscriptions@b-public.ch";	// Replace your email address

	$smtp_address   = "info@b-public.ch";		// Replace your email address
	$smtp_password	= "b-public%2015?";				// Replace your email password
	$smtp_server	= "smtp.office365.com";	// Replace your email server address

	/* Subscription Form Setup End */

	date_default_timezone_set('Etc/UTC');


	require 'phpmailer/PHPMailerAutoload.php';

	$mail = new PHPMailer();							// Create a new PHPMailer instance

	$mail->IsSMTP();									// Tell PHPMailer to use SMTP
	$mail->SMTPAuth = true;

	$mail->CharSet = "utf-8";							// Set CharSet
	$mail->Host = $smtp_server;							// Set the hostname of the mail server
	$mail->Port = 587;									// Set the SMTP port number - likely to be 25, 465 or 587
	$mail->SMTPSecure = "tls";   						// If you use gmail address, active this line
	$mail->Username = $smtp_address;					// Username to use for SMTP authentication
	$mail->Password = $smtp_password;					// Password to use for SMTP authentication

	$mail->setFrom( $mail->Username, $send_name );	// Set who the message is to be sent from
	$mail->addBCC( 'psi@b-public.ch', 'Subscriptions' );		// Set who the message is to be sent to
	$mail->addBCC( 'dja@b-public.ch', 'Subscriptions' );		// Set who the message is to be sent to
	//$mail->addAddress( $_POST["email"], $_POST["email"] );
	$mail->addAddress($_POST["email"]);
	//$mail->addAddress( $send_address2, 'Subscriptions' );
	$mail->Subject = $send_title;						// Set the subject line



	$headers = "From: $send_name <$smtp_address>\r\n".'Reply-To:'.$send_address."\r\n";
	$headers .= 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

	$mess =  '<div style="margin-bottom:20px;padding-bottom:10px;border-bottom: 1px solid #ddd;"><img src="http://www.b-public.ch/pressegespraech/img/egonz_logo_small.png" alt="Egon Zehnder"></div>';
	$mess .= '<div style="font-family:Calibri,Verdana,sans-serif; font-size:14px;"><h2>Jahres-Pressegespräch 2017</h2><h2>der Egon Zehnder</h2></div>';
	$mess .= '<div style="font-family:Calibri,Verdana,sans-serif; font-size:14px;margin-bottom:10px;">Wir bedanken uns herzlich für Ihre Anmeldung zum Jahrespresse-Gespräch 2017 der Egon Zehnder, welche wir wie folgt bestätigen:</div>';

	$mess .= '<table style="font-family:Calibri,Verdana,sans-serif;font-size:14px;">';


	$mess .= '<tr>';
	$mess .= '<td><strong>Teilnahme: </strong></td><td>'.$_POST["teilnahme"].'</td>';
	$mess .= '</tr>';

	$mess .= '<tr>';
	$mess .= '<td><strong>Unterlagen </strong></td><td>'.$_POST["unterlagen"].'</td>';
	$mess .= '</tr>';


	$mess .= '<tr>';
	$mess .= '<td><strong>Redaktion: </strong></td><td>'.$_POST["redaktion"].'</td>';
	$mess .= '</tr>';

	$mess .= '<tr>';
	$mess .= '<td><strong>Vorname: </strong></td><td>'.$_POST["vorname"].'</td>';
	$mess .= '</tr>';

	$mess .= '<tr>';
	$mess .= '<td><strong>Name: </strong></td><td>'.$_POST["name"].'</td>';
	$mess .= '</tr>';

	$mess .= '<tr>';
	$mess .= '<td><strong>PLZ: </strong></td><td>'.$_POST["plz"].'</td>';
	$mess .= '</tr>';

	$mess .= '<tr>';
	$mess .= '<td><strong>Ort: </strong></td><td>'.$_POST["ort"].'</td>';
	$mess .= '</tr>';

	$mess .= '<tr>';
	$mess .= '<td><strong>Email: </strong></td><td>'.$_POST["email"].'</td>';
	$mess .= '</tr>';

	$mess .= "</table>";
	$mess .= '<br>'.'<div style="font-family:Calibri,Verdana,sans-serif;font-size:14px;">Bei Fragen können Sie uns jederzeit gerne kontaktieren.<br><br>Wir freuen uns, Sie am Anlass begrüssen zu dürfen!</div>';

	// $message = urldecode($mess);


	//Read an HTML message body from an external file, convert referenced images to embedded,
	//convert HTML into a basic plain-text alternative body
	//$mail->msgHTML("Selected type : ".$_POST["type"]);
	$mail->msgHTML($mess);

	//send the message, check for errors
	if (!$mail->send()) {
		echo "ERROR";
		}
	else {
		echo "SUCCESS";

		//$cvsData = date("Y-m-d H:i:s") . ";" . $_POST["venue"] . ";" . $_POST["salutation"] . ";" . $_POST["surname"] . ";" . $_POST["name"] . ";" . $_POST["company"] . ";" . $_POST["phone"] . ";" . $_POST["email"] . ";" . $_POST["address"] . ";" . $_POST["plz"] . ";" . $_POST["place"] . "\n";

		function convertToWindowsCharset($string) {
			$charset =  mb_detect_encoding(
				$string,
				"UTF-8, ISO-8859-1, ISO-8859-15",
				true
			);

			$string =  mb_convert_encoding($string, "Windows-1252", $charset);
			return $string;
		}

		$cvsData = convertToWindowsCharset($cvsData);

		//$fp = fopen("formData.csv","a"); // $fp is now the file pointer to file $filename

		//if($fp){
			//fwrite($fp,$cvsData); // Write information to the file
			//fclose($fp); // Close the file
		//}

		}

?>
