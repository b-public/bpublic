<footer id="footer" role="contentinfo">
  <div class="wrapper wrapper-transparent">
    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span11 text-left small-screen-center">
          <h3 style="text-transform: lowercase;">
            b|public
            <span style="text-transform: uppercase;" class="light">
              AG
            </span>
            <span class="light">
            &nbsp;&nbsp;|&nbsp;&nbsp;

          <!--<p>-->
            +41 44 533 34 00
            &nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="mailto:info@b-public.ch">info@b-public.ch</a>
            &nbsp;&nbsp;|&nbsp;&nbsp;
            &copy; Copyright 2015
          <!--</p>--></span></h3>
        </div>

        <!-- link to impressum -->
        <div class="span1 text-right">
          <span class="light" style="font-size:17px;">
            <a href="
            <?php foreach($site->languages() as $language): ?>
              <?php e($site->language() == $language, $pages->find('impressum')->url()) ?>
            <?php endforeach ?>
            "><?php echo $pages->find('impressum')->title() ?></a>

          </span>
        </div>

      </div>
    </div>
  </div>
</footer>

<script src="<?= url('html/javascripts/jquery.min.js') ?>" type="text/javascript"></script>
<script src="<?= url('html/javascripts/bootstrap.js') ?>" type="text/javascript"></script>
<script src="<?= url('html/javascripts/jquery.flexslider-min.js') ?>" type="text/javascript"></script>
<script src="<?= url('html/javascripts/jquery.tweet.js') ?>" type="text/javascript"></script>
<script src="<?= url('html/javascripts/jquery.fancybox.pack.js') ?>" type="text/javascript"></script>
<script src="<?= url('html/javascripts/jquery.fancybox-media.js') ?>" type="text/javascript"></script>
<script src="<?= url('html/javascripts/jquery.cookie.js') ?>" type="text/javascript"></script>
<script src="<?= url('html/javascripts/script.js') ?>" type="text/javascript"></script>

<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-3317864-2', 'auto');
ga('send', 'pageview');

</script>
<!-- End Google Analytics -->
<!--
<script type="text/javascript">
$(document).ready(function() {
  $(".various").fancybox({
    maxWidth    : 800,
    maxHeight   : 600,
    fitToView   : false,
    width       : '70%',
    height      : '70%',
    autoSize    : false,
    closeClick  : false,
    openEffect  : 'elastic',
    closeEffect : 'none',
    beforeLoad  : function(){
      var url= $(this.element).attr("href");
      url = url.replace(new RegExp("watch\\?v=", "i"), 'v/');
      url += '?fs=1&autoplay=1';
      this.href = url
    },
    afterLoad   : function(){
      setTimeout( function() {$.fancybox.close(); },30000); // 3000 = 3 secs
    }
  });

  var visited = $.cookie('visited'); // create the cookie
  if (visited == 'yes') {
    return false; // second page load, cookie is active so do nothing
  } else {
    $(".various").trigger('click'); // first page load, launch fancybox
  };
  // assign cookie's value and expiration time
  $.cookie('visited', 'yes', {
    expires: 1 // the number of days the cookie will be effective
  });

});
</script>-->


</body>
