<!-- Adding Google api -->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
 <!-- Script Goes here -->
<script type="text/javascript">
 //map caonvas 1
  function initialize() {
      var styles = [
        {
            stylers: [
                { saturation: -85 }
            ]
        },{
            featureType: 'road',
            elementType: 'geometry',
            stylers: [
                { hue: "#002bff" },
                { visibility: 'simplified' }
            ]
        },{
            featureType: 'road',
            elementType: 'labels',
            stylers: [
                { visibility: 'off' }
            ]
        }
      ];

      console.log('print object: ' + JSON.stringify(styles));

    styledMap = new google.maps.StyledMapType(styles,
    {name: 'Styled Map'});


      var myLatlng = new google.maps.LatLng(25.18694, 55.2609596);
      var myLatlngBel = new google.maps.LatLng(25.18694, 55.2609596);

      var mapOptions = {
        zoom: 14,
        scrollwheel: false,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      var mapOptionsBel = {
        zoom: 14,
        scrollwheel: false,
        center: myLatlngBel,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      var map = new google.maps.Map(document.getElementById('map1'), mapOptions);
      var mapB = new google.maps.Map(document.getElementById('map2'), mapOptionsBel);

      marker = new google.maps.Marker({
        map:map,
        draggable:false,
        animation: google.maps.Animation.BOUNCE,
        position: myLatlng,
        icon: "http://oi62.tinypic.com/6fxyx5.jpg"
      });

      markerB = new google.maps.Marker({
        map:mapB,
        draggable:false,
        animation: google.maps.Animation.BOUNCE,
        position: myLatlngBel,
        icon: "http://oi62.tinypic.com/6fxyx5.jpg"
      });
        marker.setAnimation(google.maps.Animation.BOUNCE);
        markerB.setAnimation(google.mapB.Animation.BOUNCE);


      //Associate the styled map with the MapTypeId and set it to display.
      map1.mapTypes.set('map_style', styledMap);
      map1.setMapTypeId('map_style');

    }

    google.maps.event.addDomListener(window, 'load', initialize);

$("#show_map2").on('click', function(event) {
    event.preventDefault();
    /*alert('here');*/
    $("#map1").slideToggle('slow');
    $("#map2").slideToggle('slow');
});

</script>
