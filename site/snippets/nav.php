<nav class="navbar navbar-static-top">
  <div class="navbar-inner">

    <div class="container-fluid">

      <a class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>


      <div class="row-fluid">

        <div class="span3">
          <?php snippet('logo') ?>
        </div>

        <div class="row-fluid">

        <div style="padding-top:10px;" class="navbar-hidden span9 pull-right">
          <div class="nav-collapse collapse">
            <ul class="navlang inline pull-right" style="margin-bottom:5px;">
              <li><a href="
                <?php foreach($site->languages() as $language): ?>
                    <?php e($site->language() == $language, $language->url()) ?>
                <?php endforeach ?>
                ">home</a>&nbsp;&nbsp;&nbsp;|
              </li>

              <?php foreach($site->languages() as $language): ?>
              <?php // if($language != 'fr'): ?>
                <li<?php e($site->language() == $language, ' class="activeLang"') ?>>
                  <a href="<?php echo $page->url($language->code()) ?>">
                      <?php echo html($language->code()) ?>
                  </a>
                </li>
              <?php // endif ?>
              <?php endforeach ?>
            </ul>
          </div>
        </div>

          <div class="span9">
              <div class="nav-collapse collapse">
                <ul class="nav pull-right">

                  <?php
                  // find the open/active page on the first level
                  $open  = $pages->findOpen();
                  $items = ($open) ? $open->children()->visible() : false;
                  // print_r($pages->visible()->Count());
                  ?>

                  <?php foreach($pages->visible() AS $p): ?>

                    <?php $items = ($p) ? $p->children()->visible() : false; ?>

                    <?php if($items && $items->count()): ?>

                      <li class="dropdown <?php echo ($p->isOpen()) ? 'active' : '' ?>">
                        <a class="dropdown-toggle"
                           data-toggle="dropdown"
                          href="<?php echo $p->url() ?>"><?php echo html($p->title()) ?>
                        </a>
                        <ul class="dropdown-menu">
                          <?php foreach($items AS $item): ?>
                            <li>
                              <a <?php echo ($item->isOpen()) ? ' class="active"' : '' ?>
                                href="<?php echo $item->url() ?>"><?php echo html($item->title()) ?>
                              </a>
                            </li>
                          <?php endforeach ?>
                        </ul>
                      </li>

                    <?php else: ?>
                      <li <?php echo ($p->isOpen()) ? 'class="active"' : '' ?> >
                        <a href="<?php echo $p->url() ?>"><?php echo html($p->title()) ?></a>
                      </li>
                    <?php endif ?>

                  <?php endforeach ?>

                </ul>
              </div>
          </div>

        </div><!-- end row fluid-->

      </div> <!-- end row fluid-->

    </div>  <!-- end container fluid-->

  </div> <!-- end navbar inner-->
</nav>
