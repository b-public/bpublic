<div class="brand">
  <a href="
    <?php foreach($site->languages() as $language): ?>
      <?php e($site->language() == $language, $language->url()) ?>
    <?php endforeach ?>
    ">
    <img alt="b-public AG Logo" src="<?php echo url('html/images/logo.png') ?>">
  </a>
</div>
