<nav class="navbar navbar-static-top">
  <div class="navbar-inner">
    <div class="container-fluid">
      <a class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <h1 class="brand">
        <a href="index.html">
          Smart<span class="light">Box</span></a>
      </h1>


      <div class="nav-collapse collapse">

        <ul class="nav pull-right">
          <li> <a href="#"> | </a></li>
          <?php foreach(c::get('lang.available') as $lang): ?>
          <li<?php if($lang == c::get('lang.current')) echo ' class="active"' ?>>
            <a href="<?php echo $page->url($lang) ?>"><?php echo $lang ?></a>
          </li>
          <?php endforeach ?>
        </ul>
      </div>


      <div class="nav-collapse collapse">
        <ul class="nav pull-right">
          <li class=""><a href="./">Agency</a></li>
          <li class=""><a href="./">Portrait</a></li>
          <li class=""><a href="./">Services</a></li>
          <li class=""><a href="./">Consulting Services</a></li>
          <li class=""><a href="./">Ethics</a></li>
          <li class=""><a href="./">Contact</a></li>
          <!--<li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Pages</a>
            <ul class="dropdown-menu">
              <li>
                <a href="about.html">Porträt</a>
              </li>
              <li>
                <a href="services.html">Dienstleistungen</a>
              </li>
              <li>
                <a href="faq.html">Beratung</a>
              </li>
              <li>
                <a href="pricing.html">Ethik</a>
              </li>
              <li>
                <a href="docs.html">Kontact</a>
              </li>
              <li>
                <a href="testimonials.html">Testimonials</a>
              </li>
              <li>
                <a href="404.html">404</a>
              </li>
              <li>
                <a href="sidebar-right.html">Right sidebar</a>
              </li>
              <li>
                <a href="sidebar-left.html">Left sidebar</a>
              </li>
              <li>
                <a href="fullbg.html">Full background</a>
              </li>
              <li>
                <a href="altpage.html">Alternative</a>
              </li>
            </ul>
          </li>
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Features</a>
            <ul class="dropdown-menu">
              <li>
                <a href="typography.html">Typography</a>
              </li>
              <li>
                <a href="elements.html">Elements</a>
              </li>
              <li>
                <a href="icons.html">Icons</a>
              </li>
              <li>
                <a href="tables.html">Tables</a>
              </li>
            </ul>
          </li>
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Blog</a>
            <ul class="dropdown-menu">
              <li>
                <a href="blog.html">Normal blog</a>
              </li>
              <li>
                <a href="timeline.html">Timeline</a>
              </li>
              <li>
                <a href="blog-fullwidth.html">Fullwidth blog</a>
              </li>
              <li>
                <a href="post.html">Single post</a>
              </li>
            </ul>
          </li>
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Portfolio</a>
            <ul class="dropdown-menu">
              <li>
                <a href="portfolio-3col.html">Three Columns</a>
              </li>
              <li>
                <a href="portfolio-4col.html">Four Columns</a>
              </li>
              <li>
                <a href="portfolio-item.html">Single Item</a>
              </li>
              <li>
                <a href="portfolio-3col-squared.html">Squared Portfolio</a>
              </li>
            </ul>
          </li>
          <li class=""><a href="contact.html">Contact</a></li>-->
        </ul>
      </div>
    </div>
  </div>
</nav>
