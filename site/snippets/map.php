<style type="text/css">
.labels {
  color: #008879;
  font-size: 14px;
  text-align: center;
  white-space: nowrap;
  font-weight: 800;
  background-color: white;
  border: 2px solid #D0D0D0;
  padding:5px;
}
</style>

<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="<?= url('html/javascripts/markerwithlabel.js') ?>" type="text/javascript"></script>
<script>

  // var pictureLabel = document.createElement("img");
  // pictureLabel.src = "http://anda.diskstation.me/b-public/logo100.png";

  // Create an array of styles.
  var styles = [
    {
        stylers: [
            { saturation: -85 }
        ]
    },{
        featureType: 'road',
        elementType: 'geometry',
        stylers: [
            { hue: "#002bff" },
            { visibility: 'simplified' }
        ]
    },{
        featureType: 'road',
        elementType: 'labels',
        stylers: [
            { visibility: 'off' }
        ]
    }
  ],
  // put your locations lat and long here
  lat1 = 47.20088,
  lng1 = 8.5239347,
  lat2 = 47.3878883,
  lng2 = 8.5186439,
  lat3 = 46.2019982,
  lng3 = 6.1456411,
  lat4 = 50.1120678,
  lng4 = 8.6620807,
  lat5 = 48.11254,
  lng5 = 11.69011,


  // Create a new StyledMapType object, passing it the array of styles,
  // as well as the name to be displayed on the map type control.
  styledMap = new google.maps.StyledMapType(styles,
      {name: 'Styled Map'}),

  // Create a map object, and include the MapTypeId to add
  // to the map type control.
  mapOptions = {
      zoom: 13,
      scrollwheel: false,
      draggable: false,
      center: new google.maps.LatLng( lat1, lng1 ),
      mapTypeControlOptions: {
          mapTypeIds: [google.maps.MapTypeId.ROADMAP]
      }
  },
  mapOptions2 = {
      zoom: 14,
      scrollwheel: false,
      draggable: false,
      center: new google.maps.LatLng( lat2, lng2 ),
      mapTypeControlOptions: {
          mapTypeIds: [google.maps.MapTypeId.ROADMAP]
      }
  },
  mapOptions3 = {
      zoom: 16,
      scrollwheel: false,
      draggable: false,
      center: new google.maps.LatLng( lat3, lng3 ),
      mapTypeControlOptions: {
          mapTypeIds: [google.maps.MapTypeId.ROADMAP]
      }
  },
  mapOptions4 = {
      zoom: 13,
      scrollwheel: false,
      draggable: false,
      center: new google.maps.LatLng( lat4, lng4 ),
      mapTypeControlOptions: {
          mapTypeIds: [google.maps.MapTypeId.ROADMAP]
      }
  },
  mapOptions5 = {
      zoom: 13,
      scrollwheel: false,
      draggable: false,
      center: new google.maps.LatLng( lat5, lng5 ),
      mapTypeControlOptions: {
          mapTypeIds: [google.maps.MapTypeId.ROADMAP]
      }
  },
  map = new google.maps.Map(document.getElementById('map'), mapOptions),
  map2 = new google.maps.Map(document.getElementById('map2'), mapOptions2),
  map3 = new google.maps.Map(document.getElementById('map3'), mapOptions3),
  map4 = new google.maps.Map(document.getElementById('map4'), mapOptions4),
  map5 = new google.maps.Map(document.getElementById('map5'), mapOptions5)
  charlotte = new google.maps.LatLng( lat1, lng1 ),
  charlotte2 = new google.maps.LatLng( lat2, lng2 ),
  charlotte3 = new google.maps.LatLng( lat3, lng3 ),
  charlotte4 = new google.maps.LatLng( lat4, lng4 ),
  charlotte5 = new google.maps.LatLng( lat5, lng5 ),

  //marker = new google.maps.Marker({
  //  position: charlotte,
  //  map: map,
  //  title: "b|public AG Zug/Baar"  }),

  marker = new MarkerWithLabel({
    position: charlotte,
    map: map,
    labelContent: "b|public",
    labelAnchor: new google.maps.Point(35, -10),
    labelClass: "labels", // the CSS class for the label
    labelStyle: {opacity: 0.7},
    title: "b|public AG"
  });

  marker2 = new MarkerWithLabel({
    position: charlotte2,
    map: map2,
    labelContent: "b|public",
    labelAnchor: new google.maps.Point(35, -10),
    labelClass: "labels", // the CSS class for the label
    labelStyle: {opacity: 0.7},
    title: "b|public A"
  });

  marker3 = new MarkerWithLabel({
    position: charlotte3,
    map: map3,
    labelContent: "b|public",
    labelAnchor: new google.maps.Point(35, -10),
    labelClass: "labels", // the CSS class for the label
    labelStyle: {opacity: 0.7},
    title: "b|public AG"
  });

  marker4 = new MarkerWithLabel({
    position: charlotte4,
    map: map4,
    labelContent: "b|public",
    labelAnchor: new google.maps.Point(35, -10),
    labelClass: "labels", // the CSS class for the label
    labelStyle: {opacity: 0.7},
    title: "b|public AG"
  });

  marker5 = new MarkerWithLabel({
    position: charlotte5,
    map: map5,
    labelContent: "b|public",
    labelAnchor: new google.maps.Point(35, -10),
    labelClass: "labels", // the CSS class for the label
    labelStyle: {opacity: 0.7},
    title: "b|public AG"
  });

  //Associate the styled map with the MapTypeId and set it to display.
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');
  map2.mapTypes.set('map_style', styledMap);
  map2.setMapTypeId('map_style');
  map3.mapTypes.set('map_style', styledMap);
  map3.setMapTypeId('map_style');
  map4.mapTypes.set('map_style', styledMap);
  map4.setMapTypeId('map_style');
  map5.mapTypes.set('map_style', styledMap);
  map5.setMapTypeId('map_style');

  //Add Link to google maps
  google.maps.event.addListener(marker2, 'click', function() {      window.open('https://www.google.ch/maps/place/Pfingstweidstrasse+6,+Industriequartier+Hard,+8005+Z%C3%BCrich/@47.3878883,8.5186439,17z/data=!3m1!4b1!4m2!3m1!1s0x47900a3f63299e15:0x8bcdbc82598ebdeb', '_blank');
  });

  google.maps.event.addListener(marker, 'click', function() {  window.open('https://www.google.ch/maps/place/b-public+AG/@47.20088,8.523935,17z/data=!3m1!4b1!4m2!3m1!1s0x479aaa252a1b09db:0xf9b6005cc7a6afe3', '_blank');
  });

  google.maps.event.addListener(marker3, 'click', function() {  window.open('https://www.google.ch/maps/place/Grand-Rue+19,+1204+Gen%C3%A8ve/@46.2019982,6.1456411,17z/data=!3m1!4b1!4m2!3m1!1s0x478c652bfb6498f9:0x859a7e16e963df2e', '_blank');
  });

  google.maps.event.addListener(marker4, 'click', function() {
window.open('https://www.google.ch/maps/place/Westendstra%C3%9Fe+19,+60325+Frankfurt,+Deutschland/@50.1120678,8.6620807,17z/data=!3m1!4b1!4m2!3m1!1s0x47bd09541a636f9d:0x89c727943a6c9951', '_blank');
  });

  google.maps.event.addListener(marker4, 'click', function() {
window.open('https://www.google.ch/maps/place/Florastra%C3%9Fe+80B,+81827+M%C3%BCnchen,+Deutschland/@48.11254,11.69011,17z/data=!3m1!4b1!4m2!3m1!1s0x479de0320210b0eb:0x5b88030d2534975a', '_blank');
  });



</script>
