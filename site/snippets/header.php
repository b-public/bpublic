<!DOCTYPE html>
<!--[if IE 8 ]> <html lang="en" class="ie8"> <![endif]-->
<!--[if (gt IE 8)]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
  <title><?php echo html($site->title()) ?> - <?php echo html($page->title()) ?></title>
  <meta name="description" content="<?php echo html($site->description()) ?>" />
  <meta name="keywords" content="<?php echo html($site->keywords()) ?>" />
  <meta name="robots" content="index, follow" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="apple-mobile-web-app-capable" content="yes" >
  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="html/javascripts/PIE.js"></script>
  <![endif]-->
  <link href="<?= url('html/favicon.ico') ?>" rel="shortcut icon">
  <link href="<?= url('html/apple-touch-icon-144x144-precomposed.png') ?>" rel="apple-touch-icon-precomposed" sizes="144x144">
  <link href="<?= url('html/apple-touch-icon-114x114-precomposed.png') ?>" rel="apple-touch-icon-precomposed" sizes="114x114">
  <link href="<?= url('html/apple-touch-icon-72x72-precomposed.png') ?>" rel="apple-touch-icon-precomposed" sizes="72x72">
  <link href="<?= url('html/apple-touch-icon-57x57-precomposed.png') ?>" rel="apple-touch-icon-precomposed">
  <link href="<?= url('html/stylesheets/bootstrap.css') ?>" media="screen" rel="stylesheet" type="text/css" />
  <link href="<?= url('html/stylesheets/responsive.css') ?>" media="screen" rel="stylesheet" type="text/css" />
  <!--<link href="<?= url('html/stylesheets/font-awesome-all.css') ?>" media="screen" rel="stylesheet" type="text/css" />-->
  <link href="<?= url('html/stylesheets/font-awesome.css') ?>" media="screen" rel="stylesheet" type="text/css" />
  <link href="<?= url('html/stylesheets/fancybox.css') ?>" media="screen" rel="stylesheet" type="text/css" />
  <link href="<?= url('html/stylesheets/theme.css') ?>" media="screen" rel="stylesheet" type="text/css" />
  <link href="<?= url('html/fonts/fonts.css') ?>" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
