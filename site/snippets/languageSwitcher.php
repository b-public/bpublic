<div>

  <ul class="inline">
    <?php foreach(c::get('lang.available') as $lang): ?>
    <li<?php if($lang == c::get('lang.current')) echo ' class="active"' ?>>
      <a href="<?php echo $page->url($lang) ?>"><?php echo $lang ?></a>
    </li>
    <?php endforeach ?>
  </ul>
</div>
