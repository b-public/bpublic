<?php snippet('header') ?>

<div class="wrapper">

  <!-- Page Header -->
  <header id="masthead">
    <?php snippet('nav') ?>
  </header>
  <!-- Main Content -->
  <div id="content" role="main">

    <section class="section section-alt section-padded">
      <div class="container-fluid">
        <div class="section-header">
          <h1>
            <?php echo $page->subtitle1() ?>
            <small class="light"><?php echo $page->subtitle2() ?></small>
          </h1>
        </div>
        <p class="lead text-center">
          <?php echo $page->text()?>
        </p>
      </div>
    </section>

    <section class="section section-padded">
      <div class="container-fluid">
        <div class="row-fluid">

          <aside class="span3 sidebar">
            <div class="sidebar-widget widget_categories">
              <hr class="mobile-hidden" style="margin-top:0px;">
                <ul class="fa-ul">
                  <li>
                      <i class="fa-li fa fa-map-marker"></i>
                    <h3 class="sidebar-header"><?php echo $page->address1header() ?></h3>
                  </li>
                </ul>
              <hr>
                <ul class="fa-ul">
                  <li>
                      <i class="fa-li fa fa-home"></i>
                    <?php echo kirbytext($page->address1()) ?>
                  </li>
                </ul>
              <hr>
                <ul class="fa-ul">
                  <li>
                      <i class="fa-li fa fa-phone"></i>
                      <?php echo $page->address1contact() ?><br>
                      <?php echo $page->address1phone() ?>
                  </li>
                </ul>
            </div>
          </aside>

          <div class="span9">
            <div id="map"></div>
          </div>

        </div>
      </div>
    </section>

  <hr class="mobile-hidden">

    <section class="section section-padded">
      <div class="container-fluid">
        <div class="row-fluid">

          <aside class="span3 sidebar">
            <div class="sidebar-widget widget_categories">
              <hr class="mobile-hidden" style="margin-top:0px;">
                <ul class="fa-ul">
                  <li>
                      <i class="fa-li fa fa-map-marker"></i>
                    <h3 class="sidebar-header"><?php echo $page->address2header() ?></h3>
                  </li>
                </ul>
              <hr>
                <ul class="fa-ul">
                  <li>
                      <i class="fa-li fa fa-home"></i>
                    <?php echo kirbytext($page->address2()) ?>
                  </li>
                </ul>
              <hr>
                <ul class="fa-ul">
                  <li>
                      <i class="fa-li fa fa-phone"></i>
                      <?php echo $page->address2contact() ?><br>
                      <?php echo $page->address2phone() ?>
                  </li>
                </ul>
            </div>
          </aside>

          <div class="span9">
            <div id="map2"></div>
          </div>

        </div>
      </div>
    </section>

    <hr class="mobile-hidden">

    <section class="section section-padded">
      <div class="container-fluid">
        <div class="row-fluid">
          <aside class="span3 sidebar">
            <div class="sidebar-widget widget_categories">
              <hr class="mobile-hidden" style="margin-top:0px;">
                <ul class="fa-ul">
                  <li>
                      <i class="fa-li fa fa-map-marker"></i>
                    <h3 class="sidebar-header"><?php echo $page->address3header() ?></h3>
                  </li>
                </ul>
              <hr>
                <ul class="fa-ul">
                  <li>
                      <i class="fa-li fa fa-home"></i>
                    <?php echo kirbytext($page->address3()) ?>
                  </li>
                </ul>
              <hr>
                <ul class="fa-ul">
                  <li>
                      <i class="fa-li fa fa-phone"></i>
                      <?php echo $page->address3contact() ?><br>
                      <?php echo $page->address3phone() ?>
                  </li>
                </ul>
            </div>
          </aside>

          <div class="span9">
            <div id="map3"></div>
          </div>

        </div>
      </div>
    </section>

    <hr class="mobile-hidden">

    <section class="section section-padded">
      <div class="container-fluid">
        <div class="row-fluid">
          <aside class="span3 sidebar">
            <div class="sidebar-widget widget_categories">
              <hr class="mobile-hidden" style="margin-top:0px;">
                <ul class="fa-ul">
                  <li>
                      <i class="fa-li fa fa-map-marker"></i>
                    <h3 class="sidebar-header"><?php echo $page->address4header() ?></h3>
                  </li>
                </ul>
              <hr>
                <ul class="fa-ul">
                  <li>
                      <i class="fa-li fa fa-home"></i>
                    <?php echo kirbytext($page->address4()) ?>
                  </li>
                </ul>
              <hr>
                <ul class="fa-ul">
                  <li>
                      <i class="fa-li fa fa-phone"></i>
                      <?php echo $page->address4contact() ?><br>
                      <?php echo $page->address4phone() ?>
                  </li>
                </ul>
            </div>
          </aside>

          <div class="span9">
            <div id="map4"></div>
          </div>
        </div>
      </div>
    </section>


    <hr class="mobile-hidden">

    <section class="section section-padded">
      <div class="container-fluid">
        <div class="row-fluid">
          <aside class="span3 sidebar">
            <div class="sidebar-widget widget_categories">
              <hr class="mobile-hidden" style="margin-top:0px;">
                <ul class="fa-ul">
                  <li>
                      <i class="fa-li fa fa-map-marker"></i>
                    <h3 class="sidebar-header"><?php echo $page->address5header() ?></h3>
                  </li>
                </ul>
              <hr>
                <ul class="fa-ul">
                  <li>
                      <i class="fa-li fa fa-home"></i>
                    <?php echo kirbytext($page->address5()) ?>
                  </li>
                </ul>
              <hr>
                <ul class="fa-ul">
                  <li>
                      <i class="fa-li fa fa-phone"></i>
                      <?php echo $page->address5contact() ?><br>
                      <?php echo $page->address5phone() ?>
                  </li>
                </ul>
            </div>
          </aside>

          <div class="span9">
            <div id="map5"></div>
          </div>
        </div>
      </div>
    </section>

  </div>
</div>

<!-- Page Footer -->
<?php snippet('map') ?>
<?php snippet('footer') ?>
