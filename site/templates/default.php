<?php snippet('header') ?>

<!-- redirect to home if header nav-->
<?php
$openPage = $pages->findOpen()->url();
$parentPage =  $page->url();
?>

<?php if($openPage == $parentPage): ?>
  <?php $link =  $pages->find($page)->children()->first()->url() ?>
  <?php go($link) ?>
<?php else: ?>
<?php endif ?>

<div class="wrapper">
  <!-- Page Header -->
  <header id="masthead">
    <?php snippet('nav') ?>
  </header>
  <!-- Main Content -->
  <div id="content" role="main">
    <!-- Promo Section -->
    <section class="section section-alt">
      <div class="row-fluid">
        <div class="flexslider"
             data-flex-animation="fade"
             data-flex-controlsalign="center"
             data-flex-controlsposition="inside"
             data-flex-directions="hide"
             data-flex-speed="7000"
             id="intro">

            <ul class="slides">
              <li>
                <div class="super-hero-unit">
                  <figure>
                    <img alt="some image" src="<?php echo url('html/images/assets/' . $page->img()) ?>">
                  </figure>
                </div>
              </li>
            </ul>

        </div>
      </div>
    </section>

    <!-- Block -->
    <section class="section section-padded">
      <div class="container-fluid">
        <div class="row-fluid">
          <div class="span3 docs-sidebar-menu">

            <?php
            // find the open/active page on the first level
            $open  = $pages->findOpen();
            $items = ($open) ? $open->children()->visible() : false;
            // print_r($pages->visible()->Count());
            ?>

              <?php $items = ($open) ? $open->children()->visible() : false; ?>

              <?php if($items && $items->count()): ?>


                  <ul class="nav nav-list docs-sidebar-nav">
                    <?php foreach($items AS $item): ?>
                      <li <?php echo ($item->isOpen()) ? ' class="active"' : '' ?> >
                        <a href="<?php echo $item->url() ?>"><?php echo html($item->title()) ?></a>
                      </li>
                    <?php endforeach ?>
                  </ul>

              <?php else: ?>

                <li class=""><a  href="<?php echo $p->url() ?>"><?php echo html($p->title()) ?></a>

              <?php endif ?>


              <!-- add emergency call number if crisis communications is active-->
              <small><?php foreach($items AS $item): ?>

                <?php if (($item->title() == "Crisis communications" || $item->title() == "Crisis Communications" || $item->title() == "Communication de crise" || $item->title() == "Communication de crise") && $item->isOpen()): ?>
                  <hr>
                  <address style="padding-left:5px;color:rgb(229, 30, 30);font-size:16px;text-transform:uppercase;">
                  <strong><?php echo $page->emergency() ?></strong>
                  <br>
                  <i class="fa fa-phone"></i> <?php echo $page->emergencyphone() ?>
                  </address>
                <?php endif ?>

              <?php endforeach ?></small>

          </div>

          <div class="span9">
            <h2 id="docs-start-here">
              <?php echo kirbytext($page->headline()) ?>
            </h2>
            <span class="lead">
              <?php echo kirbytext($page->text()) ?>
            </span>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
</div>

<!-- Page Footer -->
<?php snippet('footer') ?>
