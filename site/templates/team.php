<?php snippet('header') ?>

<div class="wrapper">
    <!-- Page Header -->
    <header id="masthead">
      <?php snippet('nav') ?>
    </header>
    <!-- Main Content -->
    <div id="content" role="main">

        <!-- Our people -->
        <section class="section section-alt section-padded">
            <div class="container-fluid">
                <div class="section-header">
                    <h1>
                        <small style="text-transform: lowercase;" class="light">b|public</small>
                        Team
                    </h1>
                </div>

                <ul class="unstyled row-fluid">
                    <li style="padding-bottom:50px;" class="span4">
                        <div class="round-box box-big">
          <span class="box-inner">
              <img alt="some image" class="img-circle"
                   src="<?php echo url('html/images/assets/people/mba-300x300.png') ?>">
          </span>
                        </div>
                        <h3 class="text-center">
                            Markus Baumgartner
                            <small class="block">
                                Partner
                            </small>
                        </h3>

                        <ul class="inline text-center big social-icons" style="height:40px;">
                          <?php if (!$page->mbasocial1()->empty()) { ?>
                              <!-- Twitter -->
                              <li>
                                  <a data-iconcolor="#00a0d1" href="<?php echo $page->mbasocial1() ?>" target="_blank">
                                      <i class="fa fa-twitter"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->mbasocial2()->empty()) { ?>
                              <!-- Xing -->
                              <li>
                                  <a data-iconcolor="#bbc516" href="<?php echo $page->mbasocial2() ?>" target="_blank">
                                      <i class="fa fa-xing"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->mbasocial3()->empty()) { ?>
                              <!-- LinkedIn -->
                              <li>
                                  <a data-iconcolor="#0177b5" href="<?php echo $page->mbasocial3() ?>" target="_blank">
                                      <i class="fa fa-linkedin"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->mbasocial4()->empty()) { ?>
                              <!-- Email -->
                              <li>
                                  <a data-iconcolor="#008879" href=<?php echo 'mailto:' . $page->mbasocial4(); ?>>
                                      <i class="fa fa-envelope"></i>
                                  </a>
                              </li>
                          <?php } ?>
                        </ul>

                        <p style="text-align:center;">
                          <?php echo $page->mba() ?>
                        </p>

                    </li>

                    <li style="padding-bottom:50px;" class="span4">
                        <div class="round-box box-big">
          <span class="box-inner">
            <img alt="some image" class="img-circle"
                 src="<?php echo url('html/images/assets/people/man-2-300x300.jpg') ?>">
          </span>
                        </div>
                        <h3 class="text-center">
                            Thomas Egger
                            <small class="block">
                                Partner
                            </small>
                        </h3>
                        <ul class="inline text-center big social-icons" style="height:40px;">
                          <?php if (!$page->tegsocial1()->empty()) { ?>
                              <!-- Twitter -->
                              <li>
                                  <a data-iconcolor="#00a0d1" href="<?php echo $page->tegsocial1() ?>" target="_blank">
                                      <i class="fa fa-twitter"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->tegsocial2()->empty()) { ?>
                              <!-- Xing -->
                              <li>
                                  <a data-iconcolor="#bbc516" href="<?php echo $page->tegsocial2() ?>" target="_blank">
                                      <i class="fa fa-xing"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->tegsocial3()->empty()) { ?>
                              <!-- LinkedIn -->
                              <li>
                                  <a data-iconcolor="#0177b5" href="<?php echo $page->tegsocial3() ?>" target="_blank">
                                      <i class="fa fa-linkedin"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->tegsocial4()->empty()) { ?>
                              <!-- Email -->
                              <li>
                                  <a data-iconcolor="#008879" href=<?php echo 'mailto:' . $page->tegsocial4(); ?>>
                                      <i class="fa fa-envelope"></i>
                                  </a>
                              </li>
                          <?php } ?>
                        </ul>

                        <p style="text-align:center;">
                          <?php echo $page->teg() ?>
                        </p>

                    </li>

                    <li style="padding-bottom:50px;" class="span4">
                        <div class="round-box box-big">
          <span class="box-inner">
<img alt="some image" class="img-circle" src="<?php echo url('html/images/assets/people/man-3-300x300.jpg') ?>">
          </span>
                        </div>
                        <h3 class="text-center">
                            Daniel Jazbec
                            <small class="block">
                                Partner
                            </small>
                        </h3>
                        <ul class="inline text-center big social-icons" style="height:40px;">
                          <?php if (!$page->djasocial1()->empty()) { ?>
                              <!-- Twitter -->
                              <li>
                                  <a data-iconcolor="#00a0d1" href="<?php echo $page->djasocial1() ?>" target="_blank">
                                      <i class="fa fa-twitter"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->djasocial2()->empty()) { ?>
                              <!-- Xing -->
                              <li>
                                  <a data-iconcolor="#bbc516" href="<?php echo $page->djasocial2() ?>" target="_blank">
                                      <i class="fa fa-xing"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->djasocial3()->empty()) { ?>
                              <!-- LinkedIn -->
                              <li>
                                  <a data-iconcolor="#0177b5" href="<?php echo $page->djasocial3() ?>" target="_blank">
                                      <i class="fa fa-linkedin"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->djasocial4()->empty()) { ?>
                              <!-- Email -->
                              <li>
                                  <a data-iconcolor="#008879" href=<?php echo 'mailto:' . $page->djasocial4(); ?>>
                                      <i class="fa fa-envelope"></i>
                                  </a>
                              </li>
                          <?php } ?>
                        </ul>

                        <p style="text-align:center;">
                          <?php echo $page->dja() ?>
                        </p>

                    </li>

                </ul> <!-- end team block 1 -->


                <ul class="unstyled row-fluid">

                    <li style="padding-bottom:50px;" class="span4">
                        <div class="round-box box-big">
                <span class="box-inner">
                  <img alt="Pietro Simmen" class="img-circle"
                       src="<?php echo url('html/images/assets/people/psi-300x300.jpg') ?>">
                </span>
                        </div>
                        <h3 class="text-center">
                            Pietro Simmen
                            <small class="block">
                                Partner
                            </small>
                        </h3>

                        <ul class="inline text-center big social-icons" style="height:40px;">
                          <?php if (!$page->mwesocial1()->empty()) { ?>
                              <!-- Twitter -->
                              <li>
                                  <a data-iconcolor="#00a0d1" href="<?php echo $page->mwesocial1() ?>" target="_blank">
                                      <i class="fa fa-twitter"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->mwesocial2()->empty()) { ?>
                              <!-- Xing -->
                              <li>
                                  <a data-iconcolor="#bbc516" href="<?php echo $page->mwesocial2() ?>" target="_blank">
                                      <i class="fa fa-xing"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->mwesocial3()->empty()) { ?>
                              <!-- LinkedIn -->
                              <li>
                                  <a data-iconcolor="#0177b5" href="<?php echo $page->mwesocial3() ?>" target="_blank">
                                      <i class="fa fa-linkedin"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->mwesocial4()->empty()) { ?>
                              <!-- Email -->
                              <li>
                                  <a data-iconcolor="#008879" href=<?php echo 'mailto:' . $page->mwesocial4(); ?>>
                                      <i class="fa fa-envelope"></i>
                                  </a>
                              </li>
                          <?php } ?>
                        </ul>

                        <p style="text-align:center;">
                          <?php echo $page->mwe() ?>
                        </p>
                    </li>

                    <li style="padding-bottom:50px;" class="span4">
                        <div class="round-box box-big">
                <span class="box-inner">
                  <img alt="some image" class="img-circle"
                       src="<?php echo url('html/images/assets/people/kbu-300x300b.jpg') ?>">
                </span>
                        </div>
                        <h3 class="text-center">
                            Katharina Burckhardt
                            <small class="block">
                                Consultant
                            </small>
                        </h3>

                        <ul class="inline text-center big social-icons" style="height:40px;">
                          <?php if (!$page->kbusocial1()->empty()) { ?>
                              <!-- Twitter -->
                              <li>
                                  <a data-iconcolor="#00a0d1" href="<?php echo $page->kbusocial1() ?>" target="_blank">
                                      <i class="fa fa-twitter"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->kbusocial2()->empty()) { ?>
                              <!-- Xing -->
                              <li>
                                  <a data-iconcolor="#bbc516" href="<?php echo $page->kbusocial2() ?>" target="_blank">
                                      <i class="fa fa-xing"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->kbusocial3()->empty()) { ?>
                              <!-- LinkedIn -->
                              <li>
                                  <a data-iconcolor="#0177b5" href="<?php echo $page->kbusocial3() ?>" target="_blank">
                                      <i class="fa fa-linkedin"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->kbusocial4()->empty()) { ?>
                              <!-- Email -->
                              <li>
                                  <a data-iconcolor="#008879" href=<?php echo 'mailto:' . $page->kbusocial4(); ?>>
                                      <i class="fa fa-envelope"></i>
                                  </a>
                              </li>
                          <?php } ?>
                        </ul>

                        <p style="text-align:center;">
                          <?php echo $page->kbu() ?>
                        </p>
                    </li>

                    <li style="padding-bottom:50px;" class="span4">
                        <div class="round-box box-big">
                            <span class="box-inner">
                                <img alt="Sarah M&uuml;ller" class="img-circle"
                                     src="<?php echo url('html/images/assets/people/png-300x300.png') ?>">
                            </span>
                        </div>
                        <h3 class="text-center">
                            Phi Yen Nguyen
                            <small class="block">
                                Junior Consultant
                            </small>
                        </h3>

                        <ul class="inline text-center big social-icons" style="height:40px;">
                          <?php if (!$page->smusocial1()->empty()) { ?>
                              <!-- Twitter -->
                              <li>
                                  <a data-iconcolor="#00a0d1" href="<?php echo $page->pngsocial1() ?>" target="_blank">
                                      <i class="fa fa-twitter"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->pngsocial2()->empty()) { ?>
                              <!-- Xing -->
                              <li>
                                  <a data-iconcolor="#bbc516" href="<?php echo $page->pngsocial2() ?>" target="_blank">
                                      <i class="fa fa-xing"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->pngsocial3()->empty()) { ?>
                              <!-- LinkedIn -->
                              <li>
                                  <a data-iconcolor="#0177b5" href="<?php echo $page->pngsocial3() ?>" target="_blank">
                                      <i class="fa fa-linkedin"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->pngsocial4()->empty()) { ?>
                              <!-- Email -->
                              <li>
                                  <a data-iconcolor="#008879" href=<?php echo 'mailto:' . $page->pngsocial4(); ?>>
                                      <i class="fa fa-envelope"></i>
                                  </a>
                              </li>
                          <?php } ?>
                        </ul>

                        <p style="text-align:center;">
                          <?php echo $page->png() ?>
                        </p>
                    </li>


                </ul> <!-- end team block 2 -->

                <ul class="unstyled row-fluid">


                    <li style="padding-bottom:50px;" class="span4">
                        <div class="round-box box-big">
                            <span class="box-inner">
                                <img alt="Sarah M&uuml;ller" class="img-circle"
                                     src="<?php echo url('html/images/assets/people/smu-300x300.jpg') ?>">
                            </span>
                        </div>
                        <h3 class="text-center">
                            Sarah M&uuml;ller
                            <small class="block">
                                Web Project Manager
                            </small>
                        </h3>

                        <ul class="inline text-center big social-icons" style="height:40px;">
                          <?php if (!$page->smusocial1()->empty()) { ?>
                              <!-- Twitter -->
                              <li>
                                  <a data-iconcolor="#00a0d1" href="<?php echo $page->smusocial1() ?>" target="_blank">
                                      <i class="fa fa-twitter"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->smusocial2()->empty()) { ?>
                              <!-- Xing -->
                              <li>
                                  <a data-iconcolor="#bbc516" href="<?php echo $page->smusocial2() ?>" target="_blank">
                                      <i class="fa fa-xing"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->smusocial3()->empty()) { ?>
                              <!-- LinkedIn -->
                              <li>
                                  <a data-iconcolor="#0177b5" href="<?php echo $page->smusocial3() ?>" target="_blank">
                                      <i class="fa fa-linkedin"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->smusocial4()->empty()) { ?>
                              <!-- Email -->
                              <li>
                                  <a data-iconcolor="#008879" href=<?php echo 'mailto:' . $page->smusocial4(); ?>>
                                      <i class="fa fa-envelope"></i>
                                  </a>
                              </li>
                          <?php } ?>
                        </ul>

                        <p style="text-align:center;">
                          <?php echo $page->smu() ?>
                        </p>
                    </li>

                    <li style="padding-bottom:50px;" class="span4">
                        <div class="round-box box-big">
                            <span class="box-inner"><img alt="Novel Martinez" class="img-circle"
                                                         src="<?php echo url('html/images/assets/people/nma-300x300.png') ?>"></span>
                        </div>
                        <h3 class="text-center">
                            Novel Martinez
                            <small class="block">
                                Web Project Manager
                            </small>
                        </h3>

                        <ul class="inline text-center big social-icons" style="height:40px;">
                          <?php if (!$page->nmasocial1()->empty()) { ?>
                              <!-- Twitter -->
                              <li>
                                  <a data-iconcolor="#00a0d1" href="<?php echo $page->nmasocial1() ?>" target="_blank">
                                      <i class="fa fa-twitter"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->nmasocial2()->empty()) { ?>
                              <!-- Xing -->
                              <li>
                                  <a data-iconcolor="#bbc516" href="<?php echo $page->nmasocial2() ?>" target="_blank">
                                      <i class="fa fa-xing"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->nmasocial3()->empty()) { ?>
                              <!-- LinkedIn -->
                              <li>
                                  <a data-iconcolor="#0177b5" href="<?php echo $page->nmasocial3() ?>" target="_blank">
                                      <i class="fa fa-linkedin"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->nmasocial4()->empty()) { ?>
                              <!-- Email -->
                              <li>
                                  <a data-iconcolor="#008879" href=<?php echo 'mailto:' . $page->nmasocial4(); ?>>
                                      <i class="fa fa-envelope"></i>
                                  </a>
                              </li>
                          <?php } ?>
                        </ul>

                        <p style="text-align:center;">
                          <?php echo $page->nma() ?>
                        </p>

                    </li>


                    <li style="padding-bottom:50px;" class="span4">
                        <div class="round-box box-big">
                            <span class="box-inner">
                                <img alt="some image" class="img-circle"
                                     src="<?php echo url('html/images/assets/people/man-6-300x300.jpg') ?>">
                            </span>
                        </div>
                        <h3 class="text-center">
                            Stefan Riedel
                            <small class="block">
                                b-public München
                            </small>
                        </h3>

                        <ul class="inline text-center big social-icons" style="height:40px;">
                          <?php if (!$page->srisocial1()->empty()) { ?>
                              <!-- Twitter -->
                              <li>
                                  <a data-iconcolor="#00a0d1" href="<?php echo $page->srisocial1() ?>" target="_blank">
                                      <i class="fa fa-twitter"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->srisocial2()->empty()) { ?>
                              <!-- Xing -->
                              <li>
                                  <a data-iconcolor="#bbc516" href="<?php echo $page->srisocial2() ?>" target="_blank">
                                      <i class="fa fa-xing"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->srisocial3()->empty()) { ?>
                              <!-- LinkedIn -->
                              <li>
                                  <a data-iconcolor="#0177b5" href="<?php echo $page->srisocial3() ?>" target="_blank">
                                      <i class="fa fa-linkedin"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->srisocial4()->empty()) { ?>
                              <!-- Email -->
                              <li>
                                  <a data-iconcolor="#008879" href=<?php echo 'mailto:' . $page->srisocial4(); ?>>
                                      <i class="fa fa-envelope"></i>
                                  </a>
                              </li>
                          <?php } ?>
                        </ul>

                        <p style="text-align:center;">
                          <?php echo $page->sri() ?>
                        </p>
                    </li>


                </ul> <!-- end team block 3 -->
                <ul class="unstyled row-fluid">
                    <li style="padding-bottom:50px;" class="span4">
                        <div class="round-box box-big">
                            <span class="box-inner"><img alt="some image" class="img-circle"
                                                         src="<?php echo url('html/images/assets/people/man-5-300x300.jpg') ?>"></span>
                        </div>
                        <h3 class="text-center">
                            Jérôme Sicard
                            <small class="block">
                                b-public Genève
                            </small>
                        </h3>

                        <ul class="inline text-center big social-icons" style="height:40px;">
                          <?php if (!$page->jsisocial1()->empty()) { ?>
                              <!-- Twitter -->
                              <li>
                                  <a data-iconcolor="#00a0d1" href="<?php echo $page->jsisocial1() ?>" target="_blank">
                                      <i class="fa fa-twitter"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->jsisocial2()->empty()) { ?>
                              <!-- Xing -->
                              <li>
                                  <a data-iconcolor="#bbc516" href="<?php echo $page->jsisocial2() ?>" target="_blank">
                                      <i class="fa fa-xing"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->jsisocial3()->empty()) { ?>
                              <!-- LinkedIn -->
                              <li>
                                  <a data-iconcolor="#0177b5" href="<?php echo $page->jsisocial3() ?>" target="_blank">
                                      <i class="fa fa-linkedin"></i>
                                  </a>
                              </li>
                          <?php } ?>
                          <?php if (!$page->jsisocial4()->empty()) { ?>
                              <!-- Email -->
                              <li>
                                  <a data-iconcolor="#008879" href=<?php echo 'mailto:' . $page->jsisocial4(); ?>>
                                      <i class="fa fa-envelope"></i>
                                  </a>
                              </li>
                          <?php } ?>
                        </ul>

                        <p style="text-align:center;">
                          <?php echo $page->jsi() ?>
                        </p>
                    </li>

                </ul>


            </div>
        </section>

    </div>
</div>
<!-- Page Footer -->
<?php snippet('footer') ?>
