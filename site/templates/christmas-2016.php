<?php snippet('header') ?>

<div class="wrapper">
  <!-- Page Header -->
  <header id="masthead">
    <?php snippet('nav') ?>
  </header>
  <!-- Main Content -->
  <div id="content" role="main">
    <!-- Promo Section -->
    <section class="section section-alt">
      <div class="row-fluid">

          <video width="100%" height="auto" autoplay controls>
              <source src="/html/assets/03_B-Public_Weihnachtsclip_FINAL.mp4" type="video/mp4">
              Your browser does not support the video tag.
          </video>

      </div>
    </section>
  </div>
</div>
<!-- Page Footer -->
<?php snippet('footer') ?>
