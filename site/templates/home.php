<?php snippet('header') ?>

<div class="wrapper">
  <!-- Page Header -->
  <header id="masthead">
    <?php snippet('nav') ?>
  </header>
  <!-- Main Content -->
  <div id="content" role="main">
    <!-- Promo Section -->
    <section class="section section-alt">
      <div class="row-fluid">
        <div class="flexslider" data-flex-animation="fade" data-flex-controlsalign="center" data-flex-controlsposition="inside" data-flex-directions="hide" data-flex-speed="100000" id="intro">
          <ul class="slides">
            <li>
              <div class="super-hero-unit">
                <figure>
                  <img alt="some image" src="<?php echo url('html/images/assets/' . $page->img()) ?>">
                  <figcaption class="flex-caption">
                    <!--<h1 class="super">
                      Same
                      <span class="lighter">
                      </span>
                      Same...
                    </h1>-->
                  </figcaption>
                </figure>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </section>

    <!-- Our Services -->

    <section class="section section-padded">
      <div class="container-fluid">
        <div class="row-fluid">
          <div class="section-header">
            <h1>
              <?php echo $page->subtitle1() ?>
              <small class="light"><?php echo $page->subtitle2() ?></small>

            </h1>
          </div>
          <ul class="unstyled row-fluid">
            <li class="span3">
              <h3 class="text-center">
                Corporate & Financial Communications
              </h3>
              <div class="round-box round-medium">
                <a href="
                  <?php foreach($site->languages() as $language): ?>
                    <?php e($site->language() == $language, $pages->find('services/corporate-financial-communications')->url()) ?>
                  <?php endforeach ?>
                  ">
                <span class="box-inner">
                  <img alt="Corporate & Financial Communications" class="img-circle" src="<?php echo url('html/images/assets/back1.jpg') ?>">
                  <i class="fa fa-comments"></i>
                </span>
                </a>
              </div>

              <p>
                <?php echo $page->whatwedo1() ?>
              </p>
              <a class="more-link" href="
              <?php foreach($site->languages() as $language): ?>
                <?php e($site->language() == $language, $pages->find('services/corporate-financial-communications')->url()) ?>
              <?php endforeach ?>
              ">
                <strong>
                  <?php echo $page->readmorebold() ?>
                </strong>
                <?php echo $page->readmoresmall() ?>
              </a>
            </li>
            <li class="span3">
              <h3 class="text-center">Digital Affairs & Marketing Services</h3>
              <div class="round-box round-medium">
                <a href="
                <?php foreach($site->languages() as $language): ?>
                  <?php e($site->language() == $language, $pages->find('services/digial-affairs-marketing-services')->url()) ?>
                <?php endforeach ?>
                ">
                <span class="box-inner">
                  <img alt="Digital Affaris & Marketing Services" class="img-circle" src="<?php echo url('html/images/assets/back2.jpg') ?>">
                  <!--<i class="icon-aws"></i>-->
                  <i class="fa fa-camera"></i>
                </span>
                </a>
              </div>

              <p>
                <?php echo $page->whatwedo2() ?>
              </p>
              <a class="more-link" href="
              <?php foreach($site->languages() as $language): ?>
                <?php e($site->language() == $language, $pages->find('services/digial-affairs-marketing-services')->url()) ?>
              <?php endforeach ?>
              "> 
                <strong>
                  <?php echo $page->readmorebold() ?>
                </strong>
                <?php echo $page->readmoresmall() ?>
              </a>
            </li>
            <li class="span3">
              <h3 class="text-center">Crisis<br>Communications</h3>
              <div class="round-box round-medium">
                <a href="
                <?php foreach($site->languages() as $language): ?>
                  <?php e($site->language() == $language, $pages->find('services/crisis-communications')->url()) ?>
                <?php endforeach ?>
                ">
                <span class="box-inner">
                  <img alt="Crisis Communications" class="img-circle" src="<?php echo url('html/images/assets/back3.jpg') ?>">
                  <i class="fa fa-umbrella"></i>
                </span>
                </a>
              </div>

              <p>
                <?php echo $page->whatwedo3() ?>
              </p>
              <a class="more-link" href="
              <?php foreach($site->languages() as $language): ?>
                <?php e($site->language() == $language, $pages->find('services/crisis-communications')->url()) ?>
              <?php endforeach ?>
              ">
                <strong>
                  <?php echo $page->readmorebold() ?>
                </strong>
                <?php echo $page->readmoresmall() ?>
              </a>
            </li>
            <li class="span3">
              <div class="round-box round-medium">
                <h3 class="text-center">IT Solutions<br>& Advisory</h3>
                <a href="
                <?php foreach($site->languages() as $language): ?>
                  <?php e($site->language() == $language, $pages->find('services/it-solutions-advisory')->url()) ?>
                <?php endforeach ?>
                ">
                <span class="box-inner">
                  <img alt="IT Solutions & Advisory" class="img-circle" src="<?php echo url('html/images/assets/back4.jpg') ?>">
                  <i class="fa fa-cog fa-spin"></i>
                </span>
                </a>
              </div>

              <p>
                <?php echo $page->whatwedo4() ?>
              </p>
              <a class="more-link" href="
              <?php foreach($site->languages() as $language): ?>
                <?php e($site->language() == $language, $pages->find('services/it-solutions-advisory')->url()) ?>
              <?php endforeach ?>
              ">
                <strong>
                  <?php echo $page->readmorebold() ?>
                </strong>
                <?php echo $page->readmoresmall() ?>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </section>
  </div>
</div>
<!-- Page Footer -->
<?php snippet('footer') ?>
