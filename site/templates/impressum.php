<?php snippet('header') ?>

<div class="wrapper">
  <!-- Page Header -->
  <header id="masthead">
    <?php snippet('nav') ?>
  </header>
  <!-- Main Content -->
  <div id="content" role="main">
    <!-- Promo Section -->
    <section class="section section-alt">
      <div class="row-fluid">
        <div class="flexslider"
             data-flex-animation="fade"
             data-flex-controlsalign="center"
             data-flex-controlsposition="inside"
             data-flex-directions="hide"
             data-flex-speed="7000"
             id="intro">

            <ul class="slides">
              <li>
                <div class="super-hero-unit">
                  <figure>
                    <img alt="some image" src="<?php echo url('html/images/assets/' . $page->img()) ?>">
                  </figure>
                </div>
              </li>
            </ul>

        </div>
      </div>
    </section>

    <!-- Block -->
    <section class="section section-padded">
      <div class="container-fluid">
        <div class="row-fluid">
          <div class="span3 docs-sidebar-menu">
            <ul class="nav nav-list docs-sidebar-nav">
              <li class="active">
                <a href="#"><?php echo $page->title() ?></a>
              </li>
            </ul>
          </div>

          <div class="span9">
            <h2 id="docs-start-here">
              <?php echo kirbytext($page->headline()) ?>
            </h2>
            <span class="lead">
              <?php echo kirbytext($page->text()) ?>
            </span>
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
</div>

<!-- Page Footer -->
<?php snippet('footer') ?>
