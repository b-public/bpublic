<?php snippet('header') ?>

<div class="wrapper">

  <!-- Page Header -->
  <header id="masthead">
    <?php snippet('nav') ?>
  </header>
  <!-- Main Content -->
  <div id="content" role="main">
    <!-- Promo Section -->
    <section class="section section-alt">
      <div class="row-fluid">
        <div class="flexslider"
             data-flex-animation="fade"
             data-flex-controlsalign="center"
             data-flex-controlsposition="inside"
             data-flex-directions="hide"
             data-flex-speed="7000"
             id="intro">

            <ul class="slides">
              <li>
                <div class="super-hero-unit">
                  <figure>
                    <img alt="some image" src="<?php echo url('html/images/assets/' . $page->img()) ?>">
                    <figcaption class="flex-caption">
                      <!--<h1 class="super">
                        Same
                        <span class="lighter">
                        </span>
                        Same...
                      </h1>-->
                    </figcaption>
                  </figure>
                </div>
              </li>
            </ul>

        </div>
      </div>
    </section>

    <!-- Block -->
    <section class="section section-padded">
      <div class="container-fluid">
        <div class="row-fluid">
          <div class="span3 docs-sidebar-menu">

          <?php
          // find the open/active page on the first level
          $open  = $pages->findOpen();
          $items = ($open) ? $open->children()->visible() : false;
          // print_r($pages->visible()->Count());
          ?>

          <?php $items = ($open) ? $open->children()->visible() : false; ?>

          <?php if($items && $items->count()): ?>


              <ul class="nav nav-list docs-sidebar-nav">
                <?php foreach($items AS $item): ?>
                  <li <?php echo ($item->isOpen()) ? ' class="active"' : '' ?> >
                    <a href="<?php echo $item->url() ?>"><?php echo html($item->title()) ?></a>
                  </li>
                <?php endforeach ?>
              </ul>

          <?php else: ?>

            <li class=""><a  href="<?php echo $p->url() ?>"><?php echo html($p->title()) ?></a>

          <?php endif ?>

          </div>
            <div class="span9">
              <h2 id="docs-start-here">
                <?php echo kirbytext($page->headline()) ?>
              </h2>
              <span class="lead">
                <?php echo kirbytext($page->text()) ?>
              </span>
              <h2><i class="fa fa-map-marker"></i>&nbsp;<?php echo $page->location1() ?></h2>
              <img alt="some image" src="<?php echo url('html/images/assets/' . $page->location1image()) ?>"><br><br>
              <h2><i class="fa fa-map-marker"></i>&nbsp;<?php echo $page->location2() ?></h2>
              <img alt="some image" src="<?php echo url('html/images/assets/' . $page->location2image()) ?>"><br><br>
              <h2><i class="fa fa-map-marker"></i>&nbsp;<?php echo $page->location3() ?></h2>
              <img alt="some image" src="<?php echo url('html/images/assets/' . $page->location3image()) ?>"><br><br>
              <h2><i class="fa fa-map-marker"></i>&nbsp;<?php echo $page->location4() ?></h2>
              <img alt="some image" src="<?php echo url('html/images/assets/' . $page->location4image()) ?>"><br><br>
              <h2><i class="fa fa-map-marker"></i>&nbsp;<?php echo $page->location5() ?></h2>
              <img alt="some image" src="<?php echo url('html/images/assets/' . $page->location5image()) ?>"><br><br>
            </div>
          </div>
        </div>
    </section>

  </div>

</div>

<!-- Page Footer -->
<?php snippet('footer') ?>
