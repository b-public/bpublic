﻿<?php snippet('header') ?>

<script src="<?= url('content/subscription/subscription.send.js') ?>" type="text/javascript"></script>

<div class="wrapper">
  <!-- Page Header -->
  <header id="masthead">
    <?php snippet('nav') ?>
  </header>
  <!-- Main Content -->
  <div id="content" role="main">
    <!-- Promo Section -->
    <section class="section section-alt">
      <div class="row-fluid">
        <div class="flexslider"
             data-flex-animation="fade"
             data-flex-controlsalign="center"
             data-flex-controlsposition="inside"
             data-flex-directions="hide"
             data-flex-speed="7000"
             id="intro">

            <ul class="slides">
              <li>
                <div class="super-hero-unit">
                  <figure>
                    <img alt="some image" src="<?php echo url('html/images/assets/' . $page->img()) ?>">
                  </figure>
                </div>
              </li>
            </ul>

        </div>
      </div>
    </section>

    <!-- Block -->
    <section class="section section-padded">
      <div class="container-fluid">
        <div class="row-fluid">
          <div class="span3 docs-sidebar-menu">
            <ul class="nav nav-list docs-sidebar-nav">
              <li class="active">
                <a href="#"><?php echo $page->title() ?></a>
              </li>
            </ul>
          </div>

          <div class="span9">
            <div class="col-lg-12">
            	<h2 class="uppercase">Marketing Roundtable, 20. März 2017</h2>
                <p class="lead">Füllen Sie bitte das untenstehende Formular aus, um sich für den Anlass zu registrieren:</p>
            </div>
              <!-- REGISTER FORM -->
            <!--<form class="contact-form" id="contactForm" novalidate="">
              <div class="register-form col-lg-12">
                  <div class="control-group">
                      <div class="controls">
                          <label for="surname">Vorname<span style="color:red;">*<span></label><br>
                          <input type="text" name="surname" id="surname" required data-validation-required-message="Please enter your name" />
                      </div>
                  </div>

                  <div class="control-group">
                      <div class="controls">
                          <label for="name">Nachname<span style="color:red;">*<span></label><br>
                          <input type="text" name="name" id="name" required data-validation-required-message="Please enter your name" />
                      </div>
                  </div>

                  <div class="control-group">
                      <div class="controls ">
                          <label for="email">Email<span style="color:red;">*<span></label><br>
                          <input type="email" name="email" id="email"  required data-validation-required-message="Please enter your email" />
                      </div>
                  </div>

                  <div class="col-lg-12 text-center">
                      <button class="button button-big button-dark" onclick="contact_send();">Anmelden</button>
                  </div>
                </div>
              </form>--->


              <form class="contact-form" id="contactForm" novalidate>
                <div class="controls controls-row">
                  <div class="control-group span6">
                    <label for="name">Name<span style="color:red;">*<span></label>
                    <input class="span12" name="name" id="name" required data-validation-required-message="Bitte Name eintragen" placeholder="Ihr Name" type="text">
                  </div>
                  <div class="control-group span6">
                    <label for="surname">Nachname<span style="color:red;">*<span></label>
                    <input class="span12" name="surname" id="surname" required data-validation-required-message="Bitte Nachname eintragen" placeholder="Ihr Nachname" type="text">
                  </div>
                </div>
                <div class="controls controls-row">
                  <div class="control-group span12">
                    <label for="email">Email<span style="color:red;">*<span></label>
                    <input class="span12" name="email" id="email" placeholder="Ihre Emailadresse" type="email" required data-validation-required-message="Bitte Emailadresse eintragen" >
                  </div>
                </div>

                <br>
                <div class="controls controls-row">
                  <div class="control-group span12">
                    <button class="btn btn-primary" onclick="contact_send();">
                      Anmeldung senden
                    </button>
                  </div>
                </div>
              </form>

        </div>
      </div>
    </section>

  </div>
</div>

<!-- Page Footer -->
<?php snippet('footer') ?>
