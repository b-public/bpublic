<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Team
pages: true
files: true
fields:
  title:
    label: Titel
    type:  text
  mba:
    label: Markus Baumgartner
    type:  textarea
    size:  medium
  mbasocial1:
    label: Twitter
    type: text
    width: 1/2
  mbasocial2:
    label: Xing
    type: text
    width: 1/2
  mbasocial3:
    label: LinkedIn
    type: text
    width: 1/2
  mbasocial4:
    label: Email
    type: text
    width: 1/2
  teg:
    label: Thomas Egger
    type:  textarea
    size:  medium
  tegsocial1:
    label: Twitter
    type: text
    width: 1/2
  tegsocial2:
    label: Xing
    type: text
    width: 1/2
  tegsocial3:
    label: LinkedIn
    type: text
    width: 1/2
  tegsocial4:
    label: Email
    type: text
    width: 1/2
  dja:
    label: Daniel Jazbec
    type:  textarea
    size:  medium
  djasocial1:
    label: Twitter
    type: text
    width: 1/2
  djasocial2:
    label: Xing
    type: text
    width: 1/2
  djasocial3:
    label: LinkedIn
    type: text
    width: 1/2
  djasocial4:
    label: Email
    type: text
    width: 1/2
  nma:
    label: Novel Martinez
    type:  textarea
    size:  medium
  nmasocial1:
    label: Twitter
    type: text
    width: 1/2
  nmasocial2:
    label: Xing
    type: text
    width: 1/2
  nmasocial3:
    label: LinkedIn
    type: text
    width: 1/2
  nmasocial4:
    label: Email
    type: text
    width: 1/2
  jsi:
    label: Jérome Sicard
    type:  textarea
    size:  medium
  jsisocial1:
    label: Twitter
    type: text
    width: 1/2
  jsisocial2:
    label: Xing
    type: text
    width: 1/2
  jsisocial3:
    label: LinkedIn
    type: text
    width: 1/2
  jsisocial4:
    label: Email
    type: text
    width: 1/2
  sri:
    label: Stefan Riedel
    type:  textarea
    size:  medium
  srisocial1:
    label: Twitter
    type: text
    width: 1/2
  srisocial2:
    label: Xing
    type: text
    width: 1/2
  srisocial3:
    label: LinkedIn
    type: text
    width: 1/2
  srisocial4:
    label: Email
    type: text
    width: 1/2
  kbu:
    label: Katharina Burkhardt
    type:  textarea
    size:  medium
  kbusocial1:
    label: Twitter
    type: text
    width: 1/2
  kbusocial2:
    label: Xing
    type: text
    width: 1/2
  kbusocial3:
    label: LinkedIn
    type: text
    width: 1/2
  kbusocial4:
    label: Email
    type: text
    width: 1/2
  hlr:
    label: Christian Hiller von Gaertringen
    type:  textarea
    size:  medium
  hlrsocial1:
    label: Twitter
    type: text
    width: 1/2
  hlrsocial2:
    label: Xing
    type: text
    width: 1/2
  hlrsocial3:
    label: LinkedIn
    type: text
    width: 1/2
  hlrsocial4:
    label: Email
    type: text
    width: 1/2
  smu:
    label: Sarah Müller
    type:  textarea
    size:  medium
  smusocial1:
    label: Twitter
    type: text
    width: 1/2
  smusocial2:
    label: Xing
    type: text
    width: 1/2
  smusocial3:
    label: LinkedIn
    type: text
    width: 1/2
  smusocial4:
    label: Email
    type: text
    width: 1/2
  mwe:
    label: Marcel Wertli
    type:  textarea
    size:  medium
  mwesocial1:
    label: Twitter
    type: text
    width: 1/2
  mwesocial2:
    label: Xing
    type: text
    width: 1/2
  mwesocial3:
    label: LinkedIn
    type: text
    width: 1/2
  mwesocial4:
    label: Email
    type: text
    width: 1/2
  png:
    label: Phi Yen Nguyen
    type:  textarea
    size:  medium
  pngsocial1:
    label: Twitter
    type: text
    width: 1/2
  pngsocial2:
    label: Xing
    type: text
    width: 1/2
  pngsocial3:
    label: LinkedIn
    type: text
    width: 1/2
  pngsocial4:
    label: Email
    type: text
    width: 1/2