<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Default
pages: true
files: true
fields:
  img:
    label: Slider Image
    type:  text
    size:  small
  title:
    label: Titel
    type:  text
  headline:
    label: Headline
    type:  text
  text:
    label: Text
    type:  textarea
    size:  large
  subtitle:
    label: Subtitle
    type:  text
