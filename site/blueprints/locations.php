<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Locations
pages: true
files: true
fields:
  img:
    label: Slider Image
    type:  text
    size:  small
  title:
    label: Titel
    type:  text
  headline:
    label: Headline
    type:  text
  text:
    label: Text
    type:  textarea
    size:  large
  location1:
    label: Location 1 (Text)
    type:  text
    width: 1/2
  location1image:
    label: Location 1 (Image)
    type:  text
    width: 1/2
  location2:
    label: Location 2 (Text)
    type:  text
    width: 1/2
  location2image:
    label: Location 2 (Image)
    type:  text
    width: 1/2
  location3:
    label: Location 3 (Text)
    type:  text
    width: 1/2
  location3image:
    label: Location 3 (Image)
    type:  text
    width: 1/2
  location4:
    label: Location 4 (Text)
    type:  text
    width: 1/2
  location4image:
    label: Location 4 (Image)
    type:  text
    width: 1/2
  location5:
    label: Location 5 (Text)
    type:  text
    width: 1/2
  location5image:
    label: Location 5 (Image)
    type:  text
    width: 1/2
