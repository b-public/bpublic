<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Contact
pages: true
files: true
fields:
  title:
    label: Titel
    type:  text
  subtitle1:
    label: Subtitle fett
    type: text
    width: 1/2
  subtitle2:
    label: Subtitle normal
    type: text
    width: 1/2
  text:
    label: Text
    type:  textarea
    size:  large
  address1header:
    label: Adresse 1 Header
    type:  text
    width: 1/2
  address1:
    label: Adresse 1
    type:  textarea
    size:  small
    width: 1/2
  address1contact:
    label: Contact Person Address 1
    type:  text
    width: 1/2
  address1phone:
    label: Contact Phone Address 1
    type:  text
    width: 1/2
  address2header:
    label: Adresse 2 Header
    type:  text
    width: 1/2
  address2:
    label: Adresse 2
    type:  textarea
    size:  small
    width: 1/2
  address2contact:
    label: Contact Person Address 2
    type:  text
    width: 1/2
  address2phone:
    label: Contact Phone Address 2
    type:  text
    width: 1/2
  address3header:
    label: Adresse 3 Header
    type:  text
    width: 1/2
  address3:
    label: Adresse 3
    type:  textarea
    size:  small
    width: 1/2
  address3contact:
    label: Contact Person Address 3
    type:  text
    width: 1/2
  address3phone:
    label: Contact Phone Address 3
    type:  text
    width: 1/2
  address4header:
    label: Adresse 4 Header
    type:  text
    width: 1/2
  address4:
    label: Adresse 4
    type:  textarea
    size:  small
    width: 1/2
  address4contact:
    label: Contact Person Address 4
    type:  text
    width: 1/2
  address4phone:
    label: Contact Phone Address 4
    type:  text
    width: 1/2
  address5header:
    label: Adresse 5 Header
    type:  text
    width: 1/2
  address5:
    label: Adresse 5
    type:  textarea
    size:  small
    width: 1/2
  address5contact:
    label: Contact Person Address 5
    type:  text
    width: 1/2
  address5phone:
    label: Contact Phone Address 5
    type:  text
    width: 1/2
