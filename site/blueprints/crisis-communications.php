<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Crisis Communications
pages: true
files: true
fields:
  img:
    label: Slider Image
    type:  text
    size:  small
  title:
    label: Titel
    type:  text
  headline:
    label: Headline
    type:  text
  text:
    label: Text
    type:  textarea
    size:  large
  emergency:
    label: Notfalltelefon Titel
    type: text
    width: 1/2
  emergencyphone:
    label: Notfalltelefon Nummer
    type: text
    width: 1/2
  subtitle:
    label: Subtitle
    type:  text
