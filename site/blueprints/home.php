<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Home
pages: true
files: true
fields:
  img:
    label: Slider Image
    type:  text
    size:  small
  title:
    label: Titel
    type:  text
  subtitle1:
    label: Subtitle fett
    type: text
    width: 1/2
  subtitle2:
    label: Subtitle normal
    type: text
    width: 1/2
  text:
    label: Text
    type:  textarea
    size:  large
  whatwedo1:
    label: What we do Box 1
    type:  text
  whatwedo2:
    label: What we do Box 2
    type:  text
  whatwedo3:
    label: What we do Box 3
    type:  text
  whatwedo4:
    label: What we do Box 4
    type:  text
  readmorebold:
    label: Read more Link fett
    type:  text
    width: 1/2
  readmoresmall:
    label: Read more Link normal
    type:  text
    width: 1/2
