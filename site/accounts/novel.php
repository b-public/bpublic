<?php if(!defined('KIRBY')) exit ?>

username: novel
firstname: Novel
lastname: Martinez
email: nma@b-public.ch
password: >
  $2a$10$Sgeh.e8yibO30dOh8HMAwu81US.izCQSB.jeL6zllBM9fXaj3WhBO
language: en
role: admin
