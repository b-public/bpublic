<?php if(!defined('KIRBY')) exit ?>

username: madeleine
firstname:
lastname:
email: mst@b-public.ch
password: >
  $2a$10$jzCYprE7.BusSO8nO84gVOXWD20GKB1nZ03YEiuCK4LXMn3f2sdRy
language: de
role: editor
token: a20db5fea406a16f076f7ceb7274b876ad36fd2e
history:
  - consulting/reputation
  - consulting/analysis
  - agency/locations
  - agency
  - impressum
