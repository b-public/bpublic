<?php if(!defined('KIRBY')) exit ?>

username: daniel
email: dja@b-public.ch
password: >
  $2a$10$lrqUuMsiCFNSSNHNnZr8uuDvvqjhssJ6iNVYic.kgwqBpUeTbDXEy
language: de
role: admin
token: 75349f19d7f04efa2db66293aa1ecf955a3cca5e
history:
  - portrait/team
  - contact
  - subscription
  - services/crisis-communications
  - >
    services/corporate-financial-communications
