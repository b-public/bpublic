<?php if(!defined('KIRBY')) exit ?>

username: jerome
firstname: Jérome
lastname: Sicard
email: jsi@b-public.ch
password: >
  $2a$10$0HUd9xfRW/dY0u7ouLUN2eWeCWJ7qo7UtGTdzVTrZRoqTI6idEc2u
language: en
role: editor
token: bf7bb6ee71381612f5b36f509c4795e7ff90a640
history:
  - portrait/team
  - agency/locations
  - services/crisis-communications
  - >
    services/digial-affairs-marketing-services
  - consulting/references
